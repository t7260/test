const fs = require('fs');
const fetch = require('node-fetch')

var apiToken = '?api_token=4f7769439adf7ef8e482d2daef77375cd6d0158b65fcdca543b74b5c0c92&user%5Bauth_token%5D=qKok4lCx8YB9Wgp%2Bo862AA%3D%3D&user%5Bid%5D=4753230';

var ex = '';

fs.readFile('./page.txt', 'utf8', (e, d) => {
    d = d.trim();
    fetch(`https://community.gethopscotch.com/api/v2/categories/newest_featured/newest${apiToken}&page=${d}`)
    .then(res => {
        return res.json();
    }).then(d => {
        d.projects.forEach(v => {
            if(v.number_of_stars >= 100) {
                ex += `  \n - ${v.title} by ${v.user||{'nickname': '(?)'}.nickname||'(?)'} (${v.number_of_stars} likes, ${v.play_count} plays) — ${v.uuid}`;
            }
        })
        console.log(ex)
    });
})
